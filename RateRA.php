<!DOCTYPE html>
<html>
    <head>
        <?php include('php/head.php'); ?>
    </head>
    <body>
        <div class="bg"></div>
        <header>
            <?php include('php/header.php'); ?>
        </header>
        <h1 class="page-title">Rate your RA</h1>
        <section class="find-form">
            <form action="/php/listSchools.php" method="post">
                <h3>I am looking for an RA at</h3>
                <input type="text" 
                       name="school"
                       placeholder="school name">
                <button type="submit" 
                        value="rateForm" 
                        name="submit">Search</button>
            </form>
        </section>
        <footer id="contact">
            <a href="contact.php">Contact</a>
        </footer>
    </body>
</html>