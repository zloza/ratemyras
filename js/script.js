$(document).ready(function(){
    
    $(".hamburger").click(function(){
        $(".menu").toggleClass("active");
        $(this).toggleClass("open");
    });
    
    $(document).click(function(e){
        if (e.target.id != '.login-register-button' && $(e.target).parents('.login-register-button').length == 0) {
            $(".login-register-button").removeClass("expanded");
        }
    });
    
    $(".login-register-button").click(function(){
        $(this).addClass("expanded");
    });
    
    
    
});