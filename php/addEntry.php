<html>
    <head>
        <head>
        <?php include('../php/head.php'); ?>
    </head>
    <body>
        <div class="bg"></div>
<?php
$school = $_GET['school'];
$servername = "localhost";
$username = "root";
$password = "Zach1234";
$database = $school;

// Create Connection
$conn = mysqli_connect($servername, $username, $password, $database);
// Check Connection
if (!$conn) {
    die("connection failed: ".mysqli_connect_error());
} else {
    echo "<br>connected!";
}

$firstname = check_input($_POST["firstname"],'Enter first name');
$lastname = check_input($_POST["lastname"],'Enter last name');
$dorm = check_input($_POST["dorm"],'Enter dorm');
$strictness = check_input($_POST["S_rating"],'Enter rating');
$helpfulness = check_input($_POST["H_rating"],'Enter rating');
$involvement = check_input($_POST["I_rating"],'Enter rating');
$hot = check_input($_POST["hrating"],'Enter rating');
$comment = check_input($_POST["comment"],'Enter a comment');
$overall = (($strictness * (-1) + 6) + $helpfulness + $involvement)/3;

function check_input($data, $problem='')
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    if ($problem && strlen($data) == 0)
    {
        die($problem);
    }
    return $data;
}

$tbl = $firstname."_".$lastname;
$tbl = strtolower($tbl);

$sql = "SELECT 1 FROM $tbl";
$result = mysqli_query($sql);

if(empty($result)){
    $sql = "CREATE TABLE `".$tbl."` (
    school VARCHAR(100) NOT NULL default '',
    firstname VARCHAR(30) NOT NULL default '',
    lastname VARCHAR(40) NOT NULL default '',
    dorm VARCHAR(50) NOT NULL default '',
    strictness DECIMAL(2,1) NOT NULL default '0',
    helpfulness DECIMAL(2,1) NOT NULL default '0',
    involvement DECIMAL(2,1) NOT NULL default '0',
    hot DECIMAL(2,1) NOT NULL default '0',
    overall DECIMAL(2,1) NOT NULL default '0',
    comment VARCHAR(500) NOT NULL default ''
    )";
    if (mysqli_query($conn,$sql)) {
        echo "database created successfully<br>";
    } else {
        echo mysqli_error($conn)."<br>";
    }
}
$comment = str_replace("'","''",$comment);
$sql = "INSERT INTO ".$tbl." (school, firstname, lastname, dorm, strictness, helpfulness, involvement, hot, overall, comment) 
VALUES ('$school', '$firstname', '$lastname', '$dorm', '$strictness', '$helpfulness', '$involvement', '$hot', '$overall', '$comment')";

if ($conn->query($sql) == TRUE) {
    $message = "Entry recorded successfully";
} else {
    $message = "Error: " .$sql. "<br>" .$conn->error;
}
$conn->close();
?>
    <section class="find-form">
        <h3><?php echo $message; ?></h3>
    </section>
    </body>
    <script type="text/javascript">
    setTimeout(function () {
        window.location.href= 'http://ratemyras.com/'; // the redirect goes here
    },4000);
    </script>
</html>

