<!DOCTYPE html>
<html>
    <head>
        <?php include('../php/head.php'); ?>
    </head>
    <body>
        <div class="bg"></div>
            <header>
                <?php include('../php/header.php'); ?>
            </header>       
            <h1 class="page-title">Rate your RA</h1>
            <section class="find-form">
<!--                <?php include('/php/rate-form.php'); ?>-->
                <?php
                $school = $_GET["value"];
                $school = str_replace(' ', '_', $school);
                $school = strtolower($school);
                ?>
                <form method="post" action="/php/addEntry.php?school=<?php echo $school;?>">
                    <h3>RA's Name</h3>
                    <input type="text" name="firstname" placeholder="first name">
                    <input type="text" name="lastname" placeholder="last name">
                    <h3>What Dorm?</h3>
                    <input type="text" name="dorm" placeholder="dorm">
                    <h3>Strictness</h3>
                    <h3 style="text-indent: 12px;letter-spacing:13px;">1 2 3 4 5</h3>
                    <div class="radio-buttons">
                        <input type="radio" name="S_rating" value=1 />
                        <input type="radio" name="S_rating" value=2 />
                        <input type="radio" name="S_rating" value=3 />
                        <input type="radio" name="S_rating" value=4 />
                        <input type="radio" name="S_rating" <?php if (isset($S_rating) && ($S_rating==1 || $S_rating==2 || $S_rating==3 || $S_rating==4)) echo "checked";?> value=5>
                    </div>
                    <h3>Helpfulness</h3>
                    <h3 style="text-indent: 12px;letter-spacing:13px;">1 2 3 4 5</h3>
                    <div class="radio-buttons">
                        <input type="radio" name="H_rating" value=1 />
                        <input type="radio" name="H_rating" value=2 />
                        <input type="radio" name="H_rating" value=3 />
                        <input type="radio" name="H_rating" value=4 />
                        <input type="radio" name="H_rating" <?php if (isset($H_rating) && ($H_rating==1 || $H_rating==2 || $H_rating==3 || $H_rating==4)) echo "checked";?> value=5>
                    </div>
                    <h3>Involvement</h3>
                    <h3 style="text-indent: 12px;letter-spacing:13px;">1 2 3 4 5</h3>
                    <div class="radio-buttons">
                        <input type="radio" name="I_rating" value=1 />
                        <input type="radio" name="I_rating" value=2 />
                        <input type="radio" name="I_rating" value=3 />
                        <input type="radio" name="I_rating" value=4 />
                        <input type="radio" name="I_rating" <?php if (isset($I_rating) && ($I_rating==1 || $I_rating==2 || $I_rating==3 || $I_rating==4)) echo "checked";?> value=5>
                    </div>
                    <h3 style="color:red;">Hot</h3>
                    <h3 style="letter-spacing:5px;">yes no</h3>
                    <div class="radio-buttons">
                        <input type="radio" name="hrating" value=1 />
                        <input type="radio" name="hrating" <?php if (isset($hrating) && $hrating==yes) echo "checked";?> value=0 />
                    </div>
                    <h3>Comments</h3>
                    <textarea type="text" name="comment" placeholder="comments"></textarea>
                    <button type="submit" value="submit" name="submit">Done</button>
                </form>
            </section>
        <footer id="contact">
            <a href="contact.php">Contact</a>
        </footer>
    </body>
</html>