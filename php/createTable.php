<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "ras";

// Create Connection
$conn = new mysqli($servername, $username, $password, $database);
// Check Connection
if ($conn->connect_error) {
    die("connection failed: ".$conn->connect_error);
}
$school = 'California Lutheran University';
$school = str_replace(' ', '_', $school);
echo $school;

$sql = "CREATE TABLE $school (
first_name VARCHAR(30) NOT NULL,
last_name VARCHAR(30) NOT NULL,
dorm VARCHAR(50) NOT NULL,
comment VARCHAR(500) NOT NULL,
strictness INT(1),
helpfulness INT(1),
involvement INT(1),
hot VARCHAR(3),
overall FLOAT(2,1),
write_ups INT(2)
)";
    
if ($conn->query($sql) === TRUE) {
    echo "Table MyGuests created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>