<?php
// show potential errors / feedback (from login object)
if (isset($login)) {
    if ($login->errors) {
        foreach ($login->errors as $error) {
            echo $error;
        }
    }
    if ($login->messages) {
        foreach ($login->messages as $message) {
            echo $message;
        }
    }
}
?>
<h3>Register</h3>
<h4>Login</h4>
<!-- login form box -->
<form method="post" action="index.php" name="loginform">

<!--    <label for="login_input_username">Username</label>-->
    <input id="login_input_username" class="login_input" type="text" name="user_name" placeholder="email" required />

<!--    <label for="login_input_password">Password</label>-->
    <input id="login_input_password" class="login_input" type="password" name="user_password" autocomplete="off" placeholder="password" required />

    <input type="submit"  name="login" value="Log in" />

</form>

<a href="/login/register.php" class="register_new_account">Register a new account?</a>
