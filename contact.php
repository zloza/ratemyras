<!DOCTYPE html>
<html>
    <head>
        <?php include('php/head.php'); ?>
    </head>
    <body>
        <div class="bg"></div>
        <header>
            <?php include('php/header.php'); ?>
        </header>
            <h1 class="page-title">Contact us</h1>
            <section class="find-form">
                <form method="post" action="/php/mail.php">
                    <h3>Your name</h3>
                    <input type="text" name="firstname" 
                           onfocus="if(this.value == 'first name') { this.value = ''; }" 
                           value="first name" 
                           onblur="if(this.value == '') {this.value = 'first name'; }">
                    <input type="text" name="lastname" 
                           onfocus="if(this.value == 'last name') { this.value = ''; }" 
                           value="last name" 
                           onblur="if(this.value == '') {this.value = 'last name'; }">
                    <h3>Email</h3>
                    <input type="text" name="email" 
                           onfocus="if(this.value == 'email') { this.value = ''; }" 
                           value="email" 
                           onblur="if(this.value == '') {this.value = 'email'; }">
                    <h3>Comment</h3>
                    <textarea type="text" name="message" 
                              onfocus="if(this.value == 'message') { this.value = ''; }" 
                              value="message" 
                              onblur="if(this.value == '') {this.value = 'message'; }" ></textarea>
                    <button type="submit" value="submit" name="submit">Done</button>
                </form>
            </section>
        
        <footer id="contact">
            <a href="contact.php">Contact</a>
        </footer>
    </body>
</html>