<!DOCTYPE html>
<html>
    <head>
        <?php include('php/head.php'); ?>
    </head>
    <body>
        <div class="bg"></div>
        <header>
            <?php include('php/header.php'); ?>
        </header>
        <h1 class="page-title">Find an RA</h1>
        <section class="find-form">
            <form action="/php/listRAs.php" method="post">
                <h3>I'm looking for an RA named</h3>
                <input type="text" 
                       name="name"
                       placeholder="RA's name">
                <button type="submit"
                        value="submit"
                        name="submit">Search</button>
            </form>
        </section>
        <footer id="contact">
            <a href="contact.php">Contact</a>
        </footer>
    </body>
</html>