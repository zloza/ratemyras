<!DOCTYPE html>
<?php
session_start();
?>
<html>
    <head>
        <?php include_once('php/head.php'); ?>
    </head>
    <body>
        <div class="bg"></div>
        <header>
            <a class="logo" href="/index.php">
                <h1>Rate My&nbsp;</h1><h2>RAs</h2>
            </a>
            <div class="login-register-button" href="#/">
                <?php include("/login/index.php"); ?>
            </div>
        </header>
        <section class="menu homepage">
            <a href="/FindRA.php">
                <h3>Find an</h3>
                <h4>RA</h4>
            </a>
            <a href="/FindSchool.php">
                <h3>Find a</h3>
                <h4>School</h4>

            </a>
            <a href="/RateRA.php">
                <h3>Rate an</h3>
                <h4>RA</h4>
            </a>
        </section>
        <footer id="contact">
            <a href="/contact.php">Contact</a>
        </footer>
    </body>
</html>